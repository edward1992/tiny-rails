FROM ruby:2.4

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update && \
  apt-get install -y nodejs sqlite3 vim --no-install-recommends && \
  rm -rf /var/lib/apt/lists/*

ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true
ENV SECRET_KEY_BASE 3c72ec5867f5bccda8eff000a021ea0255bb95e5f9c1eae68d9269f38a0ce6f64bf4f8af7bf4362b12f6398e6255601867a626f8701cf69b54a94d3b200073a2

COPY Gemfile /usr/src/app
COPY Gemfile.lock /usr/src/app
RUN bundle config --global frozen 1
RUN bundle install --without development test

COPY . /usr/src/app
RUN bundle exec rake assets:precompile

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]